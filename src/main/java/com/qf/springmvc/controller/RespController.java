package com.qf.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 15:37
 */
@Controller
@RequestMapping("resp")
public class RespController {

    @RequestMapping("/zhuan-webapp")
    public String zhuan(){
        return "zhuan";   // request.getRequestDispatcher("web资源")
    }

    // 项目路径: http://ip:port/

    @RequestMapping("/zhuan-resource1")
    public String zhuanResource1(){
        return "forward:/test";   // request.getRequestDispatcher("路径资源")
    }

    // 项目路径: http://ip:port/test

    @RequestMapping("/zhuan-resource2")
    public String zhuanResource2(){
        return "forward:test";   // request.getRequestDispatcher("路径资源")
    }

    // http://ip:port/resp/zhuan-resource2
    // http://ip:port/resp/test

    @RequestMapping("/zhuan-resource3")
    public String zhuanResource3(){
        return "forward:http://localhost:8080/test";   // request.getRequestDispatcher("路径资源")
    }

    // http://www.baidu.com

    // 404
    //  请求没有进到Controller
    //  资源路径对不对
    //  springmvc.xml文件有没有扫描注解
    //  web.xml有没有加载配置文件

    // 404 - 会在message的位置告诉你哪个资源木有找到
    //  去webapp下找对应资源
    //  如果没有 - 添加上
    //  如果有 - 重新打包项目


    @RequestMapping("/chong-resource1")
    public void chongResource1(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(request.getContextPath() + "/test");
    }
    @RequestMapping("/chong-resource2")
    public String chongResource2(){
        return "redirect:test";   // http://localhost:8080/resp/test
    }
    @RequestMapping("/chong-resource3")
    public String chongResource3(){
        return "redirect:http://www.baidu.com";
    }




















}
