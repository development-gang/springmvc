package com.qf.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 11:36
 */
@Controller
public class TestController {

    @RequestMapping(value = "/test")
    public ModelAndView test(){
        ModelAndView mv = new ModelAndView();
        mv.addObject("test","Hello SpringMVC!!");  // request.setAttribute
        mv.setViewName("/test.jsp");  //request.getRequestDispatcher("/test.jsp").forward(req,resp);
        return mv;
    }
}
