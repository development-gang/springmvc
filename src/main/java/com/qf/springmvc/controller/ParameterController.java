package com.qf.springmvc.controller;

import com.qf.springmvc.entity.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 14:38
 * 接收参数
 */
@Controller
public class ParameterController {


    /**
     * nickName - abaaba
     * age - 23
     * birthday - 2011-11-11
     */
    @RequestMapping("/easy-parameter")
    public void easyParameter(String nickName, Integer age, @DateTimeFormat(pattern = "yyyy-MM-dd") Date birthday){
        System.out.println(nickName + "," + age + "," + birthday);
    }


    /**
     * nickName - abaaba
     * age - 23
     * birthday - 2011-11-11
     */
    @RequestMapping("/pojo-parameter")
    public void pojoParameter(User user){
        System.out.println(user);
    }

    /**
     * hobby - abaaba
     * hobby - abaaba
     * hobby - abaaba
     * hobby - abaaba
     */
    @RequestMapping("/array-parameter")
    public void arrayParameter(String[] hobby, HttpServletRequest request){
        System.out.println(hobby);
    }

}
