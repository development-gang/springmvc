package com.qf.springmvc.controller;

import com.qf.springmvc.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 16:56
 */
@Controller
@RequestMapping("json")
public class JSONController {

    /**由于JSON是key-value格式:
     *      1. 使用map接收,key-value
     *      2. POJO类,属性名-属性名
     *      3. String,直接拿到JSON字符串
     */
    @RequestMapping("/parameter")
    public void parameter(@RequestBody User user){
        System.out.println(user);
    }

    /**
     * 响应JSON数据时,排除原生的resp方式,可以返回三种形式
     * Map,POJO类,String
     */
    @ResponseBody    // 以响应体的方式,将返回结果给页面,   如果返回Map,POJO,Jackson自动帮你序列化为JSON字符串，并设置好响应头信息
    @RequestMapping(value = "/resp-json",produces = "application/json;charset=utf-8")
    public User respJson(){
        /*Map map = new HashMap();
        map.put("nickName","lucy");
        map.put("age",12);
        map.put("birthday","2011-11-11");
        return map;*/
        User user = new User();
        user.setNickName("玛卡巴卡");
        user.setAge(44);
        user.setBirthday(new Date());
        return user;
//        return "{\"nickName\":\"玛卡巴卡\",\"age\":44,\"birthday\":1616490795109}";
    }



}
