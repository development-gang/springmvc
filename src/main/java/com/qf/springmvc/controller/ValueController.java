package com.qf.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 16:23
 * 向域中存值
 */
@Controller
@RequestMapping("/value")
@SessionAttributes(value = {"sessionAttributes"})
public class ValueController {

    @RequestMapping("/model")
    public String model(Model model){
        // 向域中存值
        model.addAttribute("model","ababababababa");
        return "value";
    }
    @RequestMapping("/modelmap")
    public String model(ModelMap model){
        // 向域中存值
        model.addAttribute("modelMap","modelMapmodelMapmodelMapmodelMapmodelMap");
        return "value";
    }

    @RequestMapping("/request")
    public String req(HttpServletRequest request){
        request.setAttribute("req","reqreqreqreqreqreq");
        return "value";
    }



    @RequestMapping("/session-attributes")
    public String sessionAttributes(Model model){
        model.addAttribute("sessionAttributes","sessionAttributessessionAttributessessionAttributessessionAttributes");
        return "value";
    }

    @RequestMapping("/session")
    public String session(HttpSession session){
        session.setAttribute("session","sessionsessionsessionsessionsession");
        return "value";
    }


}
