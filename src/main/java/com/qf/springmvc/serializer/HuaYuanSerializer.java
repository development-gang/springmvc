package com.qf.springmvc.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 17:37
 */
public class HuaYuanSerializer extends JsonSerializer<String> {
    @Override
    public void serialize(String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        s = "花园宝宝-" + s;
        jsonGenerator.writeString(s);
    }
}
