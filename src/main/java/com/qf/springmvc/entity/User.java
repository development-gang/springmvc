package com.qf.springmvc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.qf.springmvc.serializer.HuaYuanSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * form - 接收页面表单数据
 * DTO - Data Transfer Object   数据传输对象  Controller - Service
 * 实体类 - 就是映射数据库的
 * VO - View Object  视图需要的数据   Controller - 页面
 *
 * @author 郑大仙丶
 * @version 1.0
 * @date 2021/3/23 14:57
 */
public class User {

    @JsonProperty(value = "nickname")   // 花园宝宝-玛卡巴卡
    @JsonSerialize(using = HuaYuanSerializer.class)
    private String nickName;   // 字段名 - nick_name   页面需要的json的key - nickname


    @JsonIgnore
    private Integer age;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;



    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
